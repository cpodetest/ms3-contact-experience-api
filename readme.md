#To update property file for db connections
Update database hos, port and instance in the property file

#To complete database set up
	
	Create following tables in Oracle RDBMS

			CREATE TABLE MS3_CONTACT
			(
				FIRST_NAME VARCHAR2(100),
				LAST_NAME VARCHAR2 (100),
				DOB DATE,
				GENDER char(1),
				TITLE VARCHAR(50),
				UNQ_ID NUMBER(10) NOT NULL, 
				CONSTRAINT contact_pk PRIMARY KEY (UNQ_ID)
			)

			CREATE TABLE MS3_ADDRESS
			(
				TYPE VARCHAR2(30),
				STREET_NUMBER VARCHAR2 (50),
				STREET VARCHAR2(100),
				UNIT VARCHAR2(50),
				CITY VARCHAR2(50),
				STATE CHAR(2),
				ZIPCODE VARCHAR2(50),
				UNQ_ID NUMBER(10),
				CONTACT_ID NUMBER(10),
				CONSTRAINT fk_contact FOREIGN KEY(CONTACT_ID) REFERENCES MS3_CONTACT(UNQ_ID)
			)


			CREATE TABLE MS3_COMMUNICATION
			(
				TYPE VARCHAR2(30),
				VALUE VARCHAR2 (100),
				PREFERRED VARCHAR2(10),
				CONTACT_ID NUMBER(10),
				UNQ_ID NUMBER(10),
				CONSTRAINT fk_contact_1 FOREIGN KEY(CONTACT_ID) REFERENCES MS3_CONTACT(UNQ_ID)
			)

#To run 
Add these properties in your command line or Studio's Runs AS (configure) -> Arguments -> VM Arguments

<I will share command details with key password in email separately.>